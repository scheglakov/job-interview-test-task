<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamsStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streams_stats', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('parse_number')->unsigned();

            $table->integer('service_user_id')->unsigned();
            $table->integer('service_game_id')->unsigned();
            $table->string('service_stream_identifier');
            $table->integer('viewer_count')->unsigned();
            $table->enum('service', ['youtube', 'twitch']);

            $table->timestamps();

            $table->index('parse_number');
            $table->index('service_game_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streams_stats');
    }
}
