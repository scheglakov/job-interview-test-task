<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitchGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitch_games', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 255);
            $table->integer('twitch_game_id')->unsigned();

            $table->timestamps();

            $table->index('twitch_game_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitch_games');
    }
}
