<?php

use Illuminate\Database\Seeder;

class TestUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table((new \App\User())->getTable())->insert([
            'name' => 'Test API',
            'email' => 'test@example.com',
            'password' => \Hash::make('test123456')
        ]);
    }
}
