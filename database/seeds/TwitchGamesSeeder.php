<?php

use Illuminate\Database\Seeder;

class TwitchGamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = [
            ['twitch_game_id' => 21779, 'name' => 'League of Legends'],
            ['twitch_game_id' => 33214, 'name' => 'Fortnite'],
            ['twitch_game_id' => 29595, 'name' => 'Dota 2'],
            ['twitch_game_id' => 511224, 'name' => 'Apex Legends'],
            ['twitch_game_id' => 504463, 'name' => 'Tom Clancy\'s The Division 2'],
            ['twitch_game_id' => 493057, 'name' => 'PLAYERUNKNOWN\'S BATTLEGROUNDS'],
            ['twitch_game_id' => 32399, 'name' => 'Counter-Strike: Global Offensive'],
            ['twitch_game_id' => 29307, 'name' => 'Path of Exile'],
            ['twitch_game_id' => 138585, 'name' => 'Hearthstone'],
        ];

        \DB::table((new \App\TwitchGames())->getTable())->insert($games);
    }
}
