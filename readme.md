## Тестовое задание

В проекте для авторизации при использовании API используется Laravel Passport.

Разворачивание проекта:

    cp .env.example .env

    composer install    

    php artisan key:generate

Далее нужно создать базу данных и прописать доступы в .env

Дальше:

    php artisan migrate

    php artisan db:seed

    php artisan passport:install

Конфиг nginx:

    server {
        listen 80;
        server_name api.my.local;
        
        root /www/path-to-project/public;

        index index.php;

        access_log /www/path-to-project/storage/logs/nginx_access.log;
        error_log /www/path-to-project/storage/logs/nginx_error.log;

        client_max_body_size 64m;

        location / {
            try_files $uri $uri/ /index.php?$args;
        }       

        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass    unix:/run/php/php7.2-fpm.sock;
            try_files $uri =404;
        }
    }

Получение списка трансляций раз в N (у меня для теста N = 5) минут запускается через крон. Используется стандартный Scheduler, для запуска надо прописать:

    * * * * * cd /path-to-project && php artisan schedule:run >> /dev/null 2>&1

На этом деплоймент завершён :)

В результате создана БД, заполнена тестовыми данными (список игр, 2 пользователя для Passport'а, тестовый пользователь API).

### Тестирование
Самое время запустить тесты и проверить, всё ли работает

    cd /path-to-project
    ./vendor/phpunit/phpunit/phpunit

### Работа API

Список игр для сбора статистики расположен в таблице twitch_games

Для получения Access токена можно использовать данные тестового пользователя:

    try {
        $http = new \GuzzleHttp\Client();
    
        $response = $http->post('http://api.my.local/api/v1/auth', [
            'form_params' => [
                'email' => 'test@example.com',
                'password' => 'test123456',
            ],
        ]);
    
        dd(json_decode((string) $response->getBody(), true));
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        echo $e->getMessage() . PHP_EOL;
    }

Получение списка трансляций:

    try {
        $http = new \GuzzleHttp\Client();
    
        $response = $http->request('GET', 'http://api.my.local/api/v1/streams', [
            'query' => [
                // 'date' => '2019-03-13 10:10:00',
                'game_id' => [
                    '21779'
                ]
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
    
        dd(json_decode((string) $response->getBody(), true));
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        echo $e->getMessage() . PHP_EOL;
    }

Получение количества смотрящих трансляции:

    try {
        $http = new \GuzzleHttp\Client();
    
        $response = $http->request('GET', 'http://api.my.local/api/v1/viewers', [
            'query' => [
                //'date' => '2019-03-13 13:10:00',
                'game_id' => [
                    29307, 504463
                ]
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
    
        dd(json_decode((string) $response->getBody(), true));
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        echo $e->getMessage() . PHP_EOL;
    }