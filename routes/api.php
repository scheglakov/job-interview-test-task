<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    [
        'namespace' => 'Api',
        'prefix' => 'v1',
    ],
    function () {
        Route::post('/auth', 'Auth\AuthController@auth')->name('api.v1.user.auth');

        Route::group([
            'middleware' => 'auth:api'
        ], function () {
            Route::get('/streams', 'StreamsController@index')->middleware('api_ip_check');
            Route::get('/viewers', 'ViewersController@index')->middleware('api_ip_check');
        });
    }
);