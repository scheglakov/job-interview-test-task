<?php

namespace App\Http\Middleware;

/**
 * Check IP access
 *
 * Class CheckAPIRestrictions
 * @package App\Http\Middleware
 */
class CheckAPIRestrictions
{
    protected $allowedIPs = [
        '127.0.0.1',
        //
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (!$this->isValidIp($request->ip())) {
            return response()->json([
                'success' => false,
                'message' => 'Access forbidden'
            ], 403);
        }

        return $next($request);
    }

    protected function isValidIp($ip)
    {
        return in_array($ip, $this->allowedIPs);
    }
}
