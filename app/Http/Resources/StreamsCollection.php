<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StreamsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item, $key) {
            return [
                'user_id' => $item->service_user_id,
                'game' => $item->game_name,
                'link' => 'https://www.twitch.tv/' . $item->service_stream_identifier,
                'viewer_count' => $item->viewer_count
            ];
        })->all();
    }
}
