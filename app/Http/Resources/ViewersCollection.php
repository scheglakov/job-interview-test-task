<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ViewersCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item, $key) {
            return [
                'game' => $item->game_name,
                'link' => 'https://www.twitch.tv/directory/game/' . $item->game_name,
                'viewer_count' => $item->viewer_count
            ];
        })->all();
    }
}
