<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\APIBaseController;

use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

use Illuminate\Http\Request;

class AuthController extends APIBaseController
{
    /**
     * Password grant client
     *
     * @var
     */
    private $client;

    public function __construct()
    {
        $this->client = Client::find(2);
    }

    /**
     * Authorize user, return tokens
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation error', $validator->errors()->all(), 422);
        }

        $params = [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => request('email'),
            'password' => request('password'),
            'scope' => '*'
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');
        return Route::dispatch($proxy);
    }
}