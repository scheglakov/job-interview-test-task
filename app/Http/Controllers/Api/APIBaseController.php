<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class APIBaseController extends Controller
{
    /**
     * Send error status with error messages
     *
     * @param string $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError(string $error, array $errorMessages = [], int $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    /**
     * Date and game_id validation
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateDateAndGameIds($request)
    {
        $validator = \Validator::make($request->all(), [
            'date' => 'nullable|date_format:"Y-m-d H:i:s"',
            'game_id' => 'required|array',
            'game_id.*' => 'required_with:game_id|numeric',
        ]);

        $validator->validate();
    }
}