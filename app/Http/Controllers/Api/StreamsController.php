<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\StreamsCollection;
use App\StreamsStat;

use Illuminate\Http\Request;

/**
 * Class StreamsController
 * @package App\Http\Controllers\Api
 */
class StreamsController extends APIBaseController
{
    public function index(Request $request)
    {
        $this->validateDateAndGameIds($request);

        $streamDate = $request->get('date') ?: date('Y-m-d H:i:s');
        $gameIds = $request->get('game_id');

        // get last parse_number before requesting date
        $parseNumber = (int)StreamsStat::query()
            ->where('created_at', '<=', $streamDate)
            ->max('parse_number');

        $streamsQuery = StreamsStat::join('twitch_games', 'streams_stats.service_game_id', '=', 'twitch_games.twitch_game_id')
            ->select('streams_stats.*', 'twitch_games.name as game_name')
            ->where('parse_number', $parseNumber)
            ->whereIn('service_game_id', $gameIds)
            ->where('service', 'twitch');

        return new StreamsCollection($streamsQuery->paginate());
    }
}