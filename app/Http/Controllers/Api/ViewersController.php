<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ViewersCollection;
use App\StreamsStat;

use Illuminate\Http\Request;

/**
 * Class ViewersController
 * @package App\Http\Controllers\Api
 */
class ViewersController extends APIBaseController
{
    public function index(Request $request)
    {
        $this->validateDateAndGameIds($request);

        $streamDate = $request->get('date') ?: date('Y-m-d H:i:s');
        $gameIds = $request->get('game_id');

        // get last parse_number before requesting date
        $parseNumber = (int)StreamsStat::query()
            ->where('created_at', '<=', $streamDate)
            ->max('parse_number');

        $streamsQuery = StreamsStat::join('twitch_games', 'streams_stats.service_game_id', '=', 'twitch_games.twitch_game_id')
            ->selectRaw('twitch_games.name as game_name, SUM(streams_stats.viewer_count) as viewer_count')
            ->where('parse_number', $parseNumber)
            ->whereIn('service_game_id', $gameIds)
            ->where('service', 'twitch')
            ->groupBy('twitch_games.name');

        return new ViewersCollection($streamsQuery->paginate());
    }
}