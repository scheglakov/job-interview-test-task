<?php

namespace App\Console\Commands;

use App\DataProviders\Twitch\GetTwitchStreams;
use App\TwitchGames;
use App\TwitchObservingGames;
use App\StreamsStat;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GetTwitchActiveStreams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get_twitch_streams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get active Twitch streams for specifying games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(date('c') . ': Start streams parsing');

        try {
            $parseDate = date('Y-m-d H:i:s');

            // получим номер последнего парсинга
            $parseNumber = (int)StreamsStat::query()->max('parse_number');
            $parseNumber++;

            $gameIds = TwitchGames::pluck('twitch_game_id')->all();

            $twitchStreams = (new GetTwitchStreams($gameIds))->getAllStreams();

            $insertStreams = [];
            foreach ($twitchStreams as $twitchStream) {
                $insertStreams[] = [
                    'parse_number' => $parseNumber,
                    'service_user_id' => $twitchStream['user_id'],
                    'service_game_id' => $twitchStream['game_id'],
                    'service_stream_identifier' => $twitchStream['user_name'],
                    'service' => 'twitch',
                    'viewer_count' => $twitchStream['viewer_count'],
                    'created_at' => $parseDate,
                ];
            }

            // bulk inserting
            foreach (array_chunk($insertStreams, 200) as $insertStreamsChunk) {
                DB::table((new StreamsStat)->getTable())->insert($insertStreamsChunk);
            }

            $this->info(date('c') . ': ' . sizeof($insertStreams) . ' streams were parsed');
        } catch (\Exception $e) {
            $this->error(date('c') . ': error while streams parsing: ' . $e->getMessage());
        }
    }
}
