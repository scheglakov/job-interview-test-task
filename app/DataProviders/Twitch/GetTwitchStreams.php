<?php

namespace App\DataProviders\Twitch;

/**
 * Get twitch streams using API
 * @see https://dev.twitch.tv/docs/api/reference/#get-streams
 *
 * Class GetTwitchStreams
 * @package App\DataProviders\Twitch
 */
class GetTwitchStreams
{
    private $gameIds;

    public function __construct(array $gameIds)
    {
        $this->gameIds = $gameIds;
    }

    /**
     * Get all streams for gameIds
     * Paginate through all pages
     *
     * @return array
     */
    public function getAllStreams()
    {
        $twitch = new Twitch(new \GuzzleHttp\Client());

        $streams = $twitch->get('streams', [
            'game_id' => $this->gameIds,
            'first' => 100
        ]);

        $result = $streams['data'];

        // paginate through results
        $page = 1;
        while (!empty($streams['pagination']['cursor'])) {
            echo "get page " . $page++ . "\n";

            $streams = $twitch->get('streams', [
                'game_id' => $this->gameIds,
                'first' => 100,
                'after' => $streams['pagination']['cursor']
            ]);

            $result = array_merge($result, $streams['data']);
        }

        return $result;
    }
}