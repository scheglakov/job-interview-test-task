<?php

namespace App\DataProviders\Twitch;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Interacting with Twitch API
 *
 * @see https://dev.twitch.tv/docs/api/guide/
 *
 * Class Twitch
 * @package App\DataProviders\Twitch
 */
class Twitch
{
    private $httpClient;
    private $accessToken;
    private $baseUrl = 'https://api.twitch.tv/helix/';

    /**
     * Number of requests left until ban
     *
     * @var integer
     */
    private $rateLimitRemaining;

    /**
     * A Unix epoch timestamp of when bucket is reset
     *
     * @var integer
     */
    private $rateLimitReset;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;

        $accessData = $this->authorize();
        $this->accessToken = $accessData['access_token'];
    }

    /**
     * Authorize and get access_token
     *
     * @return array
     */
    public function authorize()
    {
        $response = $this->httpClient->request('post', 'https://id.twitch.tv/oauth2/token', [
            'form_params' => [
                'client_id' => env('TWITCH_API_CLIENT_ID'),
                'client_secret' => env('TWITCH_API_CLIENT_SECRET'),
                'grant_type' => 'client_credentials'
            ]
        ]);

        return $this->parseResponse($response);
    }

    /**
     * GET-request to API
     *
     * @param string $endPoint
     * @param array $params
     * @return array
     */
    public function get(string $endPoint, array $params = []): array
    {
        $this->checkLimitations();

        $response = $this->httpClient->request('get', $this->baseUrl . '/' . $endPoint, [
            'query' => $params,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken
            ]
        ]);

        return $this->parseResponse($response);
    }

    /**
     * Check limitations for API
     * @see https://dev.twitch.tv/docs/api/guide/#rate-limits
     *
     * @return bool
     */
    private function checkLimitations()
    {
        if (!$this->rateLimitReset) {
            return true;
        }

        if ($this->rateLimitRemaining == 0) {
            if ($this->rateLimitReset > time()) {
                sleep($this->rateLimitReset - time() + 1);
            }
        }

        return true;
    }

    /**
     * Parse JSON result
     *
     * @param ResponseInterface $response
     * @return array
     */
    private function parseResponse(ResponseInterface $response): array
    {
        if ($response->getHeader('Ratelimit-Remaining')) {
            $this->rateLimitRemaining = $response->getHeader('Ratelimit-Remaining')[0];
        }

        if ($response->getHeader('Ratelimit-Reset')) {
            $this->rateLimitReset = $response->getHeader('Ratelimit-Reset')[0];
        }

        $data = json_decode((string)$response->getBody(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new ParseException('JSON decode error: ' . json_last_error_msg());
        }

        if (!is_array($data)) {
            throw new ParseException('Response is not an array');
        }

        return $data;
    }
}