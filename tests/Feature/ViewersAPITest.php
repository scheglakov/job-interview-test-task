<?php

namespace Tests\Feature;

use Tests\TestCase;

class ViewersAPITest extends TestCase
{
    private $accessToken;

    /**
     * Получить токен авторизации
     *
     * @return mixed
     */
    public function getAuthorizationToken()
    {
        if (!empty($this->accessToken)) {
            return $this->accessToken;
        }

        $postData = [
            'email' => env('API_TEST_USER'),
            'password' => env('API_TEST_USER_PASSWORD')
        ];

        $response = $this->json('POST', 'api/v1/auth', $postData);

        $json = json_decode($response->getContent(), true);

        $this->accessToken = $json['access_token'];

        return $this->accessToken;
    }

    /**
     * Request without access_token
     */
    public function testRequestWithoutAuthorization()
    {
        $this->json('GET', 'api/v1/viewers')
            ->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    /**
     * Request with NO game_id
     */
    public function testRequestWithoutRequiredFields()
    {
        $accessToken = $this->getAuthorizationToken();

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => "Bearer $accessToken"
        ];

        $this->json('GET', 'api/v1/viewers', [], $headers)
            ->assertStatus(422)
            ->assertJsonStructure([
                'message'
            ]);
    }

    /**
     * Request with game_id
     */
    public function testRequestWithGameId()
    {
        $accessToken = $this->getAuthorizationToken();

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => "Bearer $accessToken"
        ];

        $this->json('GET', 'api/v1/viewers', [
            'game_id' => [
                21779
            ]
        ], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'meta',
                'links'
            ]);
    }
}
